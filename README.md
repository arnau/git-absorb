# Git Absorb

Small git command to absorb changes from a branch (usually `master`) to your
current branch.

## Install

Download the [release](https://github.com/arnau/git-absorb/releases) or
install via Homebrew:

    $ brew tap arnau/git-absorb
    $ brew install git-absorb

And create a git alias:

    $ git config --global alias.absorb git-absorb

## Maintainers

* [Arnau Siches](https://github.com/arnau)

## License

Git Absorb is distributed under the terms of the [MIT license](LICENSE).
